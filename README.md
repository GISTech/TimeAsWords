# Overview
Converts an input string depicting a 24 hour time format (i.e. 00:00 - 24:00)
into time as words.


# Time Intervals:

1. Midnight
2. Noon
3. O'clock
4. Before midday
5. After midday
6. Quater past
7. Quarter to
8. Half past
9. Minutes past hour
10. Minutes to hour


# Results:

![Time As Words: Scala Object Result](images/Intellij_IDEA_TimeAsWords_Results.PNG)

Time As Words: Scala Object Output Result

![Time As Words: ScalaTest Result](images/Intellij_IDEA_TimeAsWordsTest_Results.PNG)

Time As Words: ScalaTest Results