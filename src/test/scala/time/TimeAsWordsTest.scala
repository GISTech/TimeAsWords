package time

import TimeAsWords._
import org.scalatest.FunSuite

/**
  * Created by PeterW on 6/18/2017.
  */
class TimeAsWordsTest extends FunSuite {
  test("testParseTime - 00:00") {
    assert(parseTime("00:00") == (0,0))
  }
  test("testParseTime - IllegalArgumentException") {
    val thrown = intercept[Exception] {
      parseTime("25:00")
    }
    assert(thrown.getMessage == "Input string doesn't match required format: 00:00 - 24:00")
  }
  test("testFormatTime - Midnight") {
    assert(formatTime((0,0)) == "Midnight")
  }
  test("testFormatTime - Noon") {
    assert(formatTime((12,0)) == "Noon")
  }
  test("testFormatTime - o'clock before midday") {
    assert(formatTime((8,0)) == "Eight o'clock before midday")
  }
  test("testFormatTime - o'clock after midday") {
    assert(formatTime((13,0)) == "One o'clock after midday")
  }
  test("testFormatTime - Quater past") {
    assert(formatTime((13,15)) == "Quarter past one after midday")
  }
  test("testFormatTime - Half past") {
    assert(formatTime((13,30)) == "Half past one after midday")
  }
  test("testFormatTime - Quarter to") {
    assert(formatTime((13,45)) == "Quarter to two after midday")
  }
  test("testFormatTime - Minutes past hour") {
    assert(formatTime((13,25)) == "Twenty-five minutes past one after midday")
  }
  test("testFormatTime - Minutes to hour") {
    assert(formatTime((13,35)) == "Twenty-five minutes to two after midday")
  }
  test("testFormatTime - Minute singular") {
    assert(formatTime((13,1)) == "One minute past one after midday")
  }
  test("testFormatTime - Minutes plural") {
    assert(formatTime((13,2)) == "Two minutes past one after midday")
  }
}

