package time

/**
  * Created by PeterW on 6/18/2017.
  */
object TimeAsWords extends App {
  def parseTime(hhmm: String): (Int, Int) = {
    """^([01]\d|2[0-4]):([0-5]\d)$""".r.findAllIn(hhmm).matchData foreach {
      md => return (md.group(1).toInt, md.group(2).toInt)
    }
    throw new IllegalArgumentException("Input string doesn't match required format: 00:00 - 24:00")
  }

  def formatTime(hhmm: (Int, Int)): String = {
    val englishNum = Map(
      1 -> "one", 2 -> "two", 3 -> "three", 4 -> "four", 5 -> "five",
      6 -> "six", 7 -> "seven", 8 -> "eight", 9 -> "nine", 10 -> "ten",
      11 -> "eleven", 12 -> "twelve", 13 -> "thirteen", 14 -> "fourteen",
      16 -> "sixteen", 17 -> "seventeen", 18 -> "eighteen", 19 -> "nineteen",
      20 -> "twenty", 21 -> "twenty-one", 22 -> "twenty-two",
      23 -> "twenty-three", 24 -> "twenty-four", 25 -> "twenty-five",
      26 -> "twenty-six", 27 -> "twenty-seven", 28 -> "twenty-eight",
      29 -> "twenty-nine"
    )

    def hourFmt(h: Int): String = h match {
      case 0 | 24      => "midnight"
      case 12          => "noon"
      case _ if h < 12 => englishNum(h) + " before midday"
      case _           => englishNum(h - 12) + " after midday"
    }

    def minuteFmt(m: Int): String = "%s %s".format(
      englishNum(m),
      if (m == 1) "minute" else "minutes"
    )

    def fmt(m: Int): (Int => String) = m match {
      case  0          => h => "%s".format(hourFmt(h).replaceFirst("\\s", " o'clock "))
      case 15          => "quarter past " + hourFmt(_)
      case 30          => "half past " + hourFmt(_)
      case 45          => h => "quarter to " + hourFmt(h + 1)
      case _ if m < 30 => h => "%s past %s".format(minuteFmt(m), hourFmt(h))
      case _           => h => "%s to %s".format(minuteFmt(60 - m), hourFmt(h + 1))
    }

    val (hh, mm) = hhmm
    fmt(mm)(hh).capitalize
  }

  try {
    println("Processing: " + args(0))
    println(formatTime(parseTime(args(0))))
  } catch {
    case e: IllegalArgumentException => System.err.println(e.getMessage)
      System.exit(1)
  }
}

